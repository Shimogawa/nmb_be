CREATE DATABASE IF NOT EXISTS nmb;

CREATE TABLE IF NOT EXISTS nmb.users (
    id    INT         AUTO_INCREMENT NOT NULL,
    token CHAR(20)    NOT NULL,
    name  VARCHAR(50) NOT NULL,
    recently_used     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_at        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    is_deleted        BOOLEAN NOT NULL DEFAULT false,
    PRIMARY KEY (id),
    UNIQUE (token),
    INDEX (is_deleted),
    UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS nmb.threads (
    id         INT          AUTO_INCREMENT NOT NULL,
    title      VARCHAR(256) NOT NULL,
    creator_id INT          NOT NULL,
    created_at TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    is_deleted BOOLEAN      NOT NULL DEFAULT false,
    PRIMARY KEY (id),
    INDEX (is_deleted),
    INDEX (created_at DESC),
    FOREIGN KEY (creator_id) REFERENCES nmb.users (id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE IF NOT EXISTS nmb.posts (
    id            INT       AUTO_INCREMENT NOT NULL,
    thread_id     INT       NOT NULL,
    user_id       INT       NOT NULL,
    content       TEXT      NOT NULL,
    replies_id    INT       NULL,
    created_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    is_deleted    BOOLEAN   NOT NULL DEFAULT false,
    PRIMARY KEY (id),
    INDEX (is_deleted),
    INDEX (user_id),
    INDEX (thread_id),
    FOREIGN KEY (thread_id) REFERENCES nmb.threads (id),
    FOREIGN KEY (user_id) REFERENCES nmb.users (id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;
