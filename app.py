from flask import Flask
from nekoite_be_core import register_routes
from nekoite_be_core.adapters import FlaskAdapter

from routes import routes

app = Flask(__name__)

adapter = FlaskAdapter(app)

register_routes(adapter, routes)
