from typing import Any, Dict

from flask import Response, request
from nekoite_be_core import ViewBase, fields

from biz.user_biz import get_user_by_cookie, login_user, logout_user, register_user


class UserRegisterView(ViewBase):
    methods = ["POST"]
    request_schema = {
        "invite_code": fields.String(required=True),
    }
    response_schema = {
        "token": fields.String(),
    }

    def handle_req(self, req: Dict[str, Any]) -> Any:
        return register_user(req["invite_code"])


class UserLoginView(ViewBase):
    methods = ["POST"]
    request_schema = {
        "token": fields.String(required=True),
    }

    def handle_req(self, req: Dict[str, Any]) -> Response:
        return login_user(req["token"])


class GetUserInfoView(ViewBase):
    methods = ["GET"]
    request_schema = {}  # type: ignore
    response_schema = {"name": fields.String()}

    def handle_req(self, req: Dict[str, Any]) -> Any:
        user = get_user_by_cookie()
        return {"name": user.name}


class UserLogoutView(ViewBase):
    methods = ["POST"]
    request_schema = {}  # type: ignore
    _is_json_body_ = False

    def handle_req(self, req: Dict[str, Any]) -> Response:
        return logout_user()
