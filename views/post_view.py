from typing import Any, Dict
from nekoite_be_core import ViewBase, fields

from biz.post_biz import (
    create_post,
    create_thread,
    delete_post,
    get_posts_of_thread,
    get_thread_by_id,
    get_threads,
)
from utils import clamp


REQ_PAGINATION_SCHEMA = {
    "page": fields.Integer(missing=1),
    "limit": fields.Integer(missing=20),
}


class NewThreadView(ViewBase):
    methods = ["POST"]
    request_schema = {"title": fields.String(required=True)}

    def handle_req(self, req: Dict[str, Any]) -> Any:
        return create_thread(req["title"])


class GetThreadsView(ViewBase):
    methods = ["GET"]
    request_schema = {"thread_id": fields.Integer(), **REQ_PAGINATION_SCHEMA}
    response_schema = {
        "threads": fields.Nested(
            {
                "thread_id": fields.Integer(),
                "title": fields.String(),
                "creator": fields.String(),
            },
            many=True,
        )
    }

    def handle_req(self, req: Dict[str, Any]) -> Any:
        page = req["page"]
        if page <= 0:
            page = 1
        limit = clamp(req["limit"], 0, 50)
        return get_threads(page, limit, req.get("thread_id"))


class NewPostView(ViewBase):
    methods = ["POST"]
    request_schema = {
        "thread_id": fields.Integer(required=True),
        "content": fields.String(required=True),
        "replies_id": fields.Integer(required=False),
    }

    def handle_req(self, req: Dict[str, Any]) -> Any:
        create_post(req["thread_id"], req["content"])


class GetPostsOfThreadView(ViewBase):
    methods = ["GET"]
    request_schema = {
        "thread_id": fields.Integer(required=True),
        **REQ_PAGINATION_SCHEMA,
    }
    response_schema = {
        "posts": fields.Nested(
            {
                "post_id": fields.Integer(),
                "user": fields.String(),
                "content": fields.String(),
                "replies_id": fields.Integer(),
            },
            many=True,
        )
    }

    def handle_req(self, req: Dict[str, Any]) -> Any:
        page = req["page"]
        if page <= 0:
            page = 1
        limit = clamp(req["limit"], 0, 50)
        return get_posts_of_thread(req["thread_id"], page, limit)


class DeletePostView(ViewBase):
    methods = ["POST"]
    request_schema = {
        "thread_id": fields.Integer(required=True),
        "post_id": fields.Integer(required=True),
    }

    def handle_req(self, req: Dict[str, Any]) -> Any:
        return delete_post(req["thread_id"], req["post_id"])
