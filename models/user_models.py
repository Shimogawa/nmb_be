from sqlalchemy import TIMESTAMP, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from consts import USER_NAME_MAX_LEN, USER_TOKEN_LEN
from .base import ModelBase


class User(ModelBase):
    __tablename__ = "users"

    token = Column(String(USER_TOKEN_LEN), nullable=False)
    name = Column(String(USER_NAME_MAX_LEN), nullable=False)
    recently_used = Column(TIMESTAMP(), server_default=func.now())
