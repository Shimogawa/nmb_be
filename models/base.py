from email.policy import default
from nekoite_be_core.db import make_model_base
from sqlalchemy import Column, Integer, TIMESTAMP, Boolean
from sqlalchemy.sql import func

from db import session

_Base = make_model_base(session)


class ModelBase(_Base):  # type: ignore
    __abstract__ = True

    id = Column(Integer(), primary_key=True, autoincrement=True)
    created_at = Column(TIMESTAMP(True), server_default=func.now(), nullable=False)
    updated_at = Column(
        TIMESTAMP(True),
        server_default=func.now(),
        server_onupdate=func.now(),
        nullable=False,
    )
    is_deleted = Column(Boolean(), default=False, nullable=False)
