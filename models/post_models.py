from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from consts import USER_NAME_MAX_LEN, USER_TOKEN_LEN
from .base import ModelBase
from .user_models import User


class Thread(ModelBase):
    __tablename__ = "threads"

    title = Column(String(256), nullable=False)
    creator_id = Column(Integer(), ForeignKey("users.id"), nullable=False)

    creator: User = relationship("User")


class Post(ModelBase):
    __tablename__ = "posts"

    thread_id = Column(Integer(), ForeignKey(Thread.id), nullable=False)
    user_id = Column(Integer(), ForeignKey("users.id"), nullable=False)
    content = Column(Text(), nullable=False)
    replies_id = Column(Integer(), nullable=True)

    thread: Thread = relationship(Thread)
    user: User = relationship(User)
