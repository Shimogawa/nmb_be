import string
import random
from typing import Optional

from flask import request

from consts import COOKIE_KEY


def rand_str(length: int):
    return "".join(
        random.choice(string.ascii_letters + string.digits) for _ in range(length)
    )


def get_cookie() -> Optional[str]:
    return request.cookies.get(COOKIE_KEY)


def clamp(val, mi, ma):
    if val < mi:
        return mi
    if val > ma:
        return ma
    return val


def offset(page: int, limit: int) -> int:
    return (page - 1) * limit
