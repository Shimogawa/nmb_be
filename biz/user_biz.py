from typing import Dict, List, Optional, Tuple
import uuid
from datetime import datetime

from flask import Response, jsonify, request

import cache
from consts import COOKIE_EXPIRATION, USER_TOKEN_LEN, COOKIE_KEY
from errors import UserNotFoundError, UserNotLoggedInError
from models.user_models import User

from db import dbcommit
from utils import get_cookie, rand_str


def register_user(invite_code: Optional[str]) -> dict:
    user = new_user()
    return {"token": user.token}


def login_user(token: str, uid: int = None) -> Response:
    cookie, user = create_cookie(token)
    resp = jsonify({"code": 0, "message": "", "data": {"name": user.name}})
    resp.set_cookie(COOKIE_KEY, cookie, COOKIE_EXPIRATION)
    return resp


def logout_user() -> Response:
    delete_cookie(get_cookie())
    resp = jsonify({"code": 0, "message": ""})
    resp.delete_cookie(COOKIE_KEY)
    return resp


def create_cookie(token: str) -> Tuple[str, User]:
    user = get_user_by_token(token)
    if user is None:
        raise UserNotFoundError
    c = uuid.uuid4().hex
    cache.set_user_cookie(c, user.id)
    return c, user


def get_user_by_cookie(cookie: Optional[str] = None, raise_err=False) -> Optional[User]:
    if cookie is None:
        cookie = get_cookie()
    if cookie is None:
        if raise_err:
            raise UserNotLoggedInError
        return None
    uid = cache.get_user_by_cookie(cookie)
    if uid is None:
        if raise_err:
            raise UserNotLoggedInError
        return None
    res = get_user_by_id(uid)
    if res is None and raise_err:
        raise UserNotLoggedInError
    return res


def delete_cookie(cookie: Optional[str]):
    if cookie is None:
        return
    cache.delete_user_cookie(cookie)


@dbcommit
def new_user() -> User:
    token = new_user_token()
    name = new_user_name()
    user = User(token=token, name=name)
    user.insert()
    return user


def new_user_token() -> str:
    token = rand_str(USER_TOKEN_LEN)
    while user_token_exists(token):
        token = rand_str(USER_TOKEN_LEN)
    return token


CUR_USER_NAME_LEN = 7


def new_user_name() -> str:
    name = rand_str(CUR_USER_NAME_LEN)
    while user_name_exists(name):
        name = rand_str(CUR_USER_NAME_LEN)
    return name


def user_token_exists(token: str) -> bool:
    return User.query.filter(User.token == token).count() != 0


def user_name_exists(name: str) -> bool:
    return User.query.filter(User.name == name).count() != 0


def get_user_by_token(token: str) -> Optional[User]:
    return User.query.filter(User.token == token).first()


def get_user_by_id(uid: int) -> Optional[User]:
    return User.query.filter(User.id == uid).first()


def get_users_by_ids(uids: List[int]) -> Dict[int, User]:
    users = User.query.filter(User.id.in_(uids)).all()
    return {u.id: u for u in users}
