from typing import List, Optional, Tuple
from sqlalchemy.orm import load_only
from nekoite_be_core.cache import redis_func_cache, lru_cache
import ujson

from biz.user_biz import get_user_by_cookie, get_user_by_id, get_users_by_ids
from errors import PostNotFoundError, ThreadNotFoundError
from models.post_models import Post, Thread
from db import dbcommit
from utils import offset
import cache


@dbcommit
def create_thread(title: str):
    user = get_user_by_cookie(raise_err=True)
    t = Thread(title=title, creator_id=user.id)
    t.insert()


@dbcommit
def create_post(thread_id: int, content: str, replies_id: Optional[int] = None):
    user = get_user_by_cookie(raise_err=True)
    if not check_thread_exists(thread_id):
        raise ThreadNotFoundError
    if replies_id is not None and not check_post_exists(replies_id):
        raise PostNotFoundError
    post = Post(
        thread_id=thread_id, user_id=user.id, content=content, replies_id=replies_id
    )
    post.insert()


def get_threads(page: int, limit: int, thread_id: Optional[int] = None):
    if thread_id is not None:
        res: List[Thread] = [get_thread_by_id(thread_id)]
    else:
        res = (
            Thread.query.filter(Thread.is_deleted == False)
            .order_by(Thread.created_at.desc())
            .offset(offset(page, limit))
            .limit(limit)
            .options(load_only(Thread.id, Thread.title, Thread.creator_id))
            .all()
        )
    users = get_users_by_ids([t.creator_id for t in res])
    return {
        "threads": [
            {
                "thread_id": t.id,
                "title": t.title,
                "creator": users[t.creator_id].name,
            }
            for t in res
        ]
    }


def get_posts_of_thread(thread_id: int, page: int, limit: int):
    if not check_thread_exists(thread_id):
        raise ThreadNotFoundError
    res: List[Post] = (
        Post.query.filter(Post.thread_id == thread_id, Post.is_deleted == False)
        .offset(offset(page, limit))
        .limit(limit)
        .all()
    )
    users = get_users_by_ids([p.user_id for p in res])
    return {
        "posts": [
            {
                "post_id": p.id,
                "user": users[p.user_id].name,
                "content": p.content,
                "replies_id": p.replies_id,
            }
            for p in res
        ]
    }


def delete_post(thread_id: int, post_id: int):
    user = get_user_by_cookie(raise_err=True)
    assert user is not None
    post = get_post_by_id(post_id, thread_id=thread_id)
    post.is_deleted = True
    post.commit()


def get_post_by_id(post_id: int, thread_id: Optional[int] = None) -> Post:
    q = Post.query.filter(Post.id == post_id, Post.is_deleted == False)
    if thread_id is not None:
        q = q.filter(Post.thread_id == thread_id)
    post = q.first()
    if post is None:
        raise PostNotFoundError
    return post


def check_thread_exists(thread_id: int) -> bool:
    return (
        Thread.query.filter(Thread.id == thread_id, Thread.is_deleted == False).count()
        != 0
    )


def check_post_exists(post_id: int) -> bool:
    return Post.query.filter(Post.id == post_id, Post.is_deleted == False).count() != 0


def get_thread_by_id(thread_id: int) -> Thread:
    t = Thread.query.filter(Thread.id == thread_id, Thread.is_deleted == False).first()
    if t is None:
        raise ThreadNotFoundError
    return t


# @redis_func_cache(cache.rds, "nmb", serializer=ujson, ex=500)
def get_post_page_offset(
    thread_id: int, post_id: int, num_per_page: int
) -> Tuple[int, int]:
    """returns (page, offset)"""
    c = Post.query.filter(
        Post.thread_id == thread_id, Post.id < post_id, Post.is_deleted == False
    ).count()
    pg = c // num_per_page + 1
    offs = c % num_per_page + 1
    return (pg, offs)
