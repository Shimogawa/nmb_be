from nekoite_be_core.errors import BEError


class UserNotFoundError(BEError):
    def __init__(self) -> None:
        super().__init__(code=40400, message="user not found")


class ThreadNotFoundError(BEError):
    def __init__(self) -> None:
        super().__init__(code=40401, message="thread not found")


class PostNotFoundError(BEError):
    def __init__(self) -> None:
        super().__init__(code=40402, message="post not found")


class UserNotLoggedInError(BEError):
    def __init__(self) -> None:
        super().__init__(code=40001, message="user not logged in")
