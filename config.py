from typing import Any, Dict
import yaml


def read_config(filename: str):
    with open(filename, "r") as f:
        return yaml.safe_load(f)


config: Dict[str, Any] = read_config("config.yml")
