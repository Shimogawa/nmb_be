from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from nekoite_be_core.db import make_commit_decorator

import config

engine = create_engine(config.config["db_url"], echo=True)
session = scoped_session(sessionmaker(engine))

dbcommit = make_commit_decorator(session)
