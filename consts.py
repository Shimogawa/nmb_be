from datetime import timedelta

USER_TOKEN_LEN = 20
USER_NAME_MAX_LEN = 50

COOKIE_KEY = "nmb"
COOKIE_EXPIRATION = timedelta(days=3)
