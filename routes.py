from views.post_view import (
    DeletePostView,
    GetPostsOfThreadView,
    GetThreadsView,
    NewPostView,
    NewThreadView,
)
from views.user_view import (
    GetUserInfoView,
    UserLoginView,
    UserLogoutView,
    UserRegisterView,
)

routes = {
    "/user/register": UserRegisterView,
    "/user/login": UserLoginView,
    "/user/info": GetUserInfoView,
    "/user/logout": UserLogoutView,
    "/thread/new": NewThreadView,
    "/thread/get": GetThreadsView,
    "/thread/get/posts": GetPostsOfThreadView,
    "/post/new": NewPostView,
    "/post/delete": DeletePostView,
}
