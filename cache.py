from typing import Optional
import redis

import config
import consts

rds = redis.Redis(
    host=config.config.get("host", "localhost"), port=config.config.get("port", 6379)
)

USER_COOKIE_PREFIX = "nmb_cookie_"


def set_user_cookie(cookie: str, uid: int):
    rds.set(f"{USER_COOKIE_PREFIX}{cookie}", uid, ex=consts.COOKIE_EXPIRATION)


def get_user_by_cookie(cookie: str) -> Optional[int]:
    res = rds.get(f"{USER_COOKIE_PREFIX}{cookie}")
    if res is None:
        return None
    return int(res)


def delete_user_cookie(cookie: str):
    rds.delete(f"{USER_COOKIE_PREFIX}{cookie}")
